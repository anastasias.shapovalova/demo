INSERT INTO selfbooster.customer( uuid,nickname, is_active) VALUES ('2d64c2fa-1818-4761-babb-db0b257fcddb','Nastia', true);
INSERT INTO selfbooster.customer( uuid, nickname, is_active) VALUES ('3d64c2fa-1818-4761-babb-db0b257fcddb','Vania', true);

INSERT INTO selfbooster.purpose_categories( uuid, category_name) VALUES ('4d64c2fa-1818-4761-babb-db0b257fcddb', 'Здоровье');

INSERT INTO selfbooster.purpose( uuid, description, percent_completion, customer_uuid, category_uuid)
VALUES ('5d64c2fa-1818-4761-babb-db0b257fcddb', 'Похудеть на 20 кг', 10, '2d64c2fa-1818-4761-babb-db0b257fcddb', '4d64c2fa-1818-4761-babb-db0b257fcddb');

INSERT INTO selfbooster.purpose( uuid, description, percent_completion, customer_uuid, category_uuid)
VALUES ('6d64c2fa-1818-4761-babb-db0b257fcddb', 'Набрать массу 10 кг', 5, '3d64c2fa-1818-4761-babb-db0b257fcddb', '4d64c2fa-1818-4761-babb-db0b257fcddb');


INSERT INTO selfbooster.task( description, percent , purpose_uuid) VALUES ('Пробежка каждый день', 10, '5d64c2fa-1818-4761-babb-db0b257fcddb');

INSERT INTO selfbooster.task( description, percent , purpose_uuid) VALUES ('Качадка каждый день', 5, '6d64c2fa-1818-4761-babb-db0b257fcddb');
