CREATE SCHEMA IF NOT EXISTS selfbooster;
CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE  TABLE IF NOT EXISTS selfbooster.customer (
                                       uuid                 uuid  DEFAULT uuid_generate_v4 (),
                                       nickname             varchar(100)   ,
                                       is_active            boolean DEFAULT true  ,
                                       CONSTRAINT pk_customer_id PRIMARY KEY ( uuid )
);

COMMENT ON TABLE selfbooster.customer IS 'Таблица пользователей';

CREATE  TABLE IF NOT EXISTS selfbooster.purpose_categories (
                                                 uuid                 uuid  DEFAULT uuid_generate_v4 (),
                                                 category_name        varchar(100)   ,
                                                 CONSTRAINT "pk_purpose categories_id" PRIMARY KEY ( uuid )
);

CREATE  TABLE IF NOT EXISTS selfbooster.purpose (
                                      uuid                 uuid  DEFAULT uuid_generate_v4 (),
                                      description          text   ,
                                      created_at           date DEFAULT CURRENT_DATE  ,
                                      desired_at           date   ,
                                      due_at               date   ,
                                      percent_completion   float8   ,
                                      customer_uuid        uuid   ,
                                      category_uuid        uuid   ,
                                      CONSTRAINT pk_purpose_id PRIMARY KEY ( uuid ),
                                      CONSTRAINT unq_purpose_userid UNIQUE ( customer_uuid )
);

COMMENT ON TABLE selfbooster.purpose IS 'Таблица целей';

COMMENT ON COLUMN selfbooster.purpose.percent_completion IS 'процент выполнения';

CREATE  TABLE IF NOT EXISTS selfbooster.task(
                                   uuid                 uuid  DEFAULT uuid_generate_v4 (),
                                   parent_task_uuid     uuid   NULL,
                                   description          text   ,
                                   created_at           date DEFAULT CURRENT_DATE  ,
                                   desired_at           date   ,
                                   due_at               date   ,
                                   percent              float8   ,
                                   purpose_uuid         uuid   ,
                                   CONSTRAINT pk_tasks_id PRIMARY KEY ( uuid ),
                                   CONSTRAINT "unq_tasks_purpose id" UNIQUE ( purpose_uuid )
);

COMMENT ON COLUMN selfbooster.task.parent_task_uuid IS 'uuid родительской задачи';

COMMENT ON COLUMN selfbooster.task.percent IS 'процент выполнения';

ALTER TABLE selfbooster.purpose ADD CONSTRAINT "fk_purpose_purpose categories" FOREIGN KEY ( category_uuid ) REFERENCES selfbooster.purpose_categories( uuid ) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE selfbooster.purpose ADD CONSTRAINT fk_purpose_customer FOREIGN KEY ( customer_uuid ) REFERENCES selfbooster.customer( uuid );

ALTER TABLE selfbooster.task ADD CONSTRAINT fk_tasks_purpose FOREIGN KEY ( purpose_uuid ) REFERENCES selfbooster.purpose( uuid );
