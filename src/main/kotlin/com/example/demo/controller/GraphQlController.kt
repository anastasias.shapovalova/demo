package com.example.demo.controller

import com.example.demo.resolver.CustomerResolver
import com.example.demo.resolver.PurposeCategoryResolver
import com.example.demo.resolver.PurposeResolver
import com.example.demo.resolver.TaskResolver
import graphql.ExecutionInput
import graphql.ExecutionResult
import graphql.GraphQL
import graphql.analysis.MaxQueryComplexityInstrumentation
import graphql.analysis.MaxQueryDepthInstrumentation
import graphql.execution.instrumentation.ChainedInstrumentation
import io.leangen.graphql.GraphQLSchemaGenerator
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.bind.annotation.RestController
import java.util.logging.Logger
import javax.servlet.http.HttpServletRequest



@RestController
class GraphQlController (
    customerResolver: CustomerResolver,
    purposeResolver: PurposeResolver,
    taskResolver: TaskResolver,
    purposeCategoryResolver: PurposeCategoryResolver
) {
    private val graphQL: GraphQL
    @PostMapping(
        value = ["/graphql"],
        consumes = [MediaType.APPLICATION_JSON_VALUE],
        produces = [MediaType.APPLICATION_JSON_VALUE]
    )
    @ResponseBody
    fun indexFromAnnotated(@RequestBody request: Map<String?, String?>, raw: HttpServletRequest?): Map<String, Any> {
        val executionResult: ExecutionResult = graphQL.execute(
            ExecutionInput.newExecutionInput()
                .query(request["query"])
                .operationName(request["operationName"])
                .context(raw)
                .build()
        )
        return executionResult.toSpecification()
    }

    companion object {
        private val LOGGER: org.slf4j.Logger? = LoggerFactory.getLogger(GraphQlController::class.java)
    }

    init {

        //Schema generated from query classes
        val schema = GraphQLSchemaGenerator()
            .withBasePackages("com.example.demo")
            .withOperationsFromSingletons(customerResolver, purposeResolver, taskResolver, purposeCategoryResolver)
            .generate()
        graphQL = GraphQL.newGraphQL(schema)
           /* .instrumentation(
                ChainedInstrumentation(listOf(
                MaxQueryComplexityInstrumentation(15),
                MaxQueryDepthInstrumentation(3)))
            )*/
            .build()
        LOGGER?.info("Generated GraphQL schema using SPQR")
    }
}