package com.example.demo.configuration

import com.zaxxer.hikari.HikariDataSource
import org.jooq.DSLContext
import org.jooq.SQLDialect
import org.jooq.conf.RenderNameStyle
import org.jooq.conf.Settings
import org.jooq.impl.DataSourceConnectionProvider
import org.jooq.impl.DefaultConfiguration
import org.jooq.impl.DefaultDSLContext
import org.jooq.impl.DefaultExecuteListenerProvider
import org.springframework.boot.autoconfigure.jooq.JooqExceptionTranslator
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.jdbc.datasource.TransactionAwareDataSourceProxy
import javax.sql.DataSource


@Configuration
class JooqConfig(private val dataSource: DataSource) {

    @Bean
    fun dsl(): DefaultDSLContext {
        return DefaultDSLContext(configuration())
    }

    fun configuration(): DefaultConfiguration {
        val jooqConfiguration = DefaultConfiguration()
        jooqConfiguration.set(SQLDialect.POSTGRES)
        jooqConfiguration.set(Settings().withRenderNameStyle(RenderNameStyle.AS_IS))
        jooqConfiguration.set(connectionProvider())
        jooqConfiguration
            .set(DefaultExecuteListenerProvider(JooqExceptionTranslator()))
        return jooqConfiguration
    }

@Bean
fun connectionProvider(): DataSourceConnectionProvider? {
    return DataSourceConnectionProvider(
        TransactionAwareDataSourceProxy(dataSource)
    )
}

}