package com.example.demo.resolver

import com.example.demo.model.CustomerModel
import com.example.demo.service.CustomerService
import io.leangen.graphql.annotations.GraphQLArgument
import io.leangen.graphql.annotations.GraphQLComplexity
import io.leangen.graphql.annotations.GraphQLQuery
import org.springframework.stereotype.Component
import java.util.*

@Component
class CustomerResolver(private  val customerService: CustomerService){

    @GraphQLQuery(name = "customers")
    fun getCustomers(): List<CustomerModel>{
        return customerService.getAll()
    }

    @GraphQLQuery(name = "customer")
    @GraphQLComplexity("14")
    fun getCustomer(@GraphQLArgument(name = "uuid") customerUuid: UUID): CustomerModel?{
        return customerService.getById(customerUuid)
    }
}