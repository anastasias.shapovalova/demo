package com.example.demo.resolver

import com.example.demo.model.PurposeCategoriesModel
import io.leangen.graphql.annotations.GraphQLArgument
import io.leangen.graphql.annotations.GraphQLMutation
import org.springframework.stereotype.Component
import java.util.*

@Component
class PurposeCategoryResolver {
    @GraphQLMutation(name = "addPurposeCategory")
    fun addPurposeCategory(
        @GraphQLArgument(name = "customerUuid") customerUUID: UUID?,
        @GraphQLArgument(name = "purposeUuid") purposeUuid: UUID?,
        @GraphQLArgument(name = "categoryName") categoryName: String
    ): PurposeCategoriesModel {
        return PurposeCategoriesModel(
            UUID.fromString("2d64c2fa-1818-4761-babb-db0b257fcddb"),
            "Здоровье")
    }
}