package com.example.demo.resolver

import com.coxautodev.graphql.tools.GraphQLQueryResolver
import com.example.demo.model.CustomerModel
import com.example.demo.model.PurposeModel
import com.example.demo.service.PurposeService
import io.leangen.graphql.annotations.GraphQLComplexity
import io.leangen.graphql.annotations.GraphQLContext
import io.leangen.graphql.annotations.GraphQLQuery
import org.springframework.stereotype.Component

@Component
class PurposeResolver(private  val purposeService: PurposeService){

    @GraphQLQuery(name = "purposes")
    fun getPurposes(@GraphQLContext customerModel: CustomerModel): List<PurposeModel>{
       return purposeService.getAllByCustomer(customerModel.uuid)
    }
}