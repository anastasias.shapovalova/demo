package com.example.demo.resolver

import com.example.demo.model.PurposeModel
import com.example.demo.model.TasksModel
import com.example.demo.service.TaskService
import io.leangen.graphql.annotations.GraphQLContext
import io.leangen.graphql.annotations.GraphQLQuery
import org.springframework.boot.autoconfigure.batch.BatchDataSource
import org.springframework.stereotype.Component

@Component
class TaskResolver(private val taskService: TaskService) {

   /* @GraphQLQuery(name = "allTasks")
    fun getAllTasks(): List<TasksModel> {
        return taskService.getAll()
    }*/

   @GraphQLQuery(name = "tasks")
    fun getTasks(@GraphQLContext purposeModel: PurposeModel): List<TasksModel> {
        return taskService.getAllByPurpose(purposeModel.uuid)
    }

}










