package com.example.demo.repository

import com.example.demo.model.PurposeModel
import nu.studer.sample.Tables.PURPOSE
import org.springframework.stereotype.Repository
import java.util.*

@Repository
class PurposeRepository: AbstractRepository() {
    fun findOneById(uuid: UUID): PurposeModel? {
        return dsl.selectFrom(PURPOSE)
            .where(PURPOSE.UUID.eq(uuid))
            .fetchOneInto(PurposeModel::class.java)
    }

    fun findAll(): List<PurposeModel> {
        return dsl.selectFrom(PURPOSE)
            .fetchInto(PurposeModel::class.java)
    }

    fun findAllByCustomerUuid(uuid: UUID): List<PurposeModel> {
        return dsl.selectFrom(PURPOSE)
            .where(PURPOSE.CUSTOMER_UUID.eq(uuid))
            .fetchInto(PurposeModel::class.java)
    }

}