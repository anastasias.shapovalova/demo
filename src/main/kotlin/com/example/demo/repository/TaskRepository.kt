package com.example.demo.repository

import com.example.demo.model.TasksModel
import org.springframework.stereotype.Repository
import java.util.*

@Repository
class TaskRepository: AbstractRepository() {

    fun findAllByPurposeUuid(purposeUuid: UUID): List<TasksModel>{
        return dsl.select()
            .from(TASK)
            .where(TASK.PURPOSE_UUID.eq(purposeUuid))
            .fetchInto(TasksModel::class.java)
    }

    fun findOneByPurposeUuid(purposeUuid: UUID): TasksModel?{
        return dsl.select()
            .from(TASK)
            .where(TASK.PURPOSE_UUID.eq(purposeUuid))
            .fetchOneInto(TasksModel::class.java)
    }

    fun findAll(): List<TasksModel>{
        return dsl.selectFrom(TASK)
            .fetchInto(TasksModel::class.java)
    }
}