package com.example.demo.repository

import nu.studer.sample.Tables
import org.jooq.DSLContext
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.springframework.beans.factory.annotation.Autowired


abstract class AbstractRepository {
    val TASK = Tables.TASK
    val CUSTOMER = Tables.CUSTOMER

    /**
     * Logger
     */
    protected val log: Logger = LoggerFactory.getLogger(javaClass)

    /**
     * JOOQ context
     */
    @Autowired
    protected lateinit var dsl: DSLContext
}