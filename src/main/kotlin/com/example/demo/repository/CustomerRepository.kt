package com.example.demo.repository

import com.example.demo.model.CustomerModel
import nu.studer.sample.tables.records.CustomerRecord
import org.springframework.stereotype.Repository
import java.util.*

@Repository
class CustomerRepository: AbstractRepository() {
  
    fun findOneById(uuid: UUID): CustomerModel? {
        return dsl.selectFrom(CUSTOMER)
            .where(CUSTOMER.UUID.eq(uuid))
            .fetchOneInto(CustomerModel::class.java)
    }



    fun findAll(): List<CustomerModel> {
        return dsl.select(CUSTOMER.UUID)
            .fetchInto(CustomerModel::class.java)
    }

}