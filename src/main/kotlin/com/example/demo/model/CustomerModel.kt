package com.example.demo.model

import io.leangen.graphql.annotations.types.GraphQLType
import java.util.*

@GraphQLType(name = "customer")
data class CustomerModel(
    val uuid: UUID,
    val nickname: String,
    val isActive: Boolean
)