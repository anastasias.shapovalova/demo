package com.example.demo.model

import java.sql.Date
import java.util.*

data class TasksModel(
    val uuid: UUID,
    val parentTaskUuid: UUID?,
    val description: String,
    val createdAt: Date,
    val desiredAt: Date?,
    val dueAt: Date?,
    val percent: Double,
    val purposeUuid: UUID
)