package com.example.demo.model

import com.fasterxml.jackson.annotation.JsonIgnore
import io.leangen.graphql.annotations.GraphQLIgnore
import io.leangen.graphql.annotations.types.GraphQLType
import java.sql.Date
import java.util.*

@GraphQLType(name = "Purpose")
data class PurposeModel(
    @get:GraphQLIgnore
    val uuid: UUID,
    val description: String,
    val createdAt: Date,
    val desiredAt: Date?,
    val dueAt: Date?,
    val percentCompletion: Double,
    val customerUuid: UUID,
    val categoryUuid: UUID,
    val tasks: List<TasksModel>?
)