package com.example.demo.model

import io.leangen.graphql.annotations.types.GraphQLType
import java.util.*

@GraphQLType(name = "categories")
data class PurposeCategoriesModel(
    val id: UUID,
    val categoryName: String
)