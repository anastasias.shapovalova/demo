package com.example.demo.service

import com.example.demo.model.CustomerModel
import com.example.demo.repository.CustomerRepository
import org.springframework.stereotype.Service
import java.util.*

@Service
class CustomerService(
    private val customerRepository: CustomerRepository
) {
   fun getById(uuid: UUID): CustomerModel?{
       return customerRepository.findOneById(uuid)
   }
    fun getAll(): List<CustomerModel>{
        return customerRepository.findAll()
    }
}