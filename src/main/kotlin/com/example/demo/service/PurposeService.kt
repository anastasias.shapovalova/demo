package com.example.demo.service

import com.example.demo.model.PurposeModel
import com.example.demo.repository.PurposeRepository
import org.springframework.stereotype.Service
import java.util.*

@Service
class PurposeService (
    private val purposeRepository: PurposeRepository
) {
    fun getById(uuid: UUID): PurposeModel? {
        return purposeRepository.findOneById(uuid)
    }

    fun getAll(): List<PurposeModel> {
        return purposeRepository.findAll()
    }

    fun getAllByCustomer(customerUUID: UUID): List<PurposeModel> {
        return purposeRepository.findAllByCustomerUuid(customerUUID)
    }
}