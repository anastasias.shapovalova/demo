package com.example.demo.service

import com.example.demo.model.TasksModel
import com.example.demo.repository.TaskRepository
import org.springframework.stereotype.Service
import java.util.*

@Service
class TaskService(
    private val taskRepository: TaskRepository
) {

    fun getAll(): List<TasksModel> {
        return taskRepository.findAll()
    }

    fun getAllByPurpose(purposeUUID: UUID): List<TasksModel> {
        return taskRepository.findAllByPurposeUuid(purposeUUID)
    }

    fun getOneByPurpose(purposeUUID: UUID): TasksModel? {
        return taskRepository.findOneByPurposeUuid(purposeUUID)
    }
}